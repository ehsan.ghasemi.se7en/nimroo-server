<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title', 'summary', 'description',
        'media_id', 'code', 'price',
        'inventory','productcat_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function gallery()
    {
        return $this->belongsToMany('App\Media')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Productcat', 'productcat_id');
    }

    /**
     * Product Cover photo
     */
    public function cover()
    {
        return $this->belongsTo('App\Media', 'media_id');
    }
}
