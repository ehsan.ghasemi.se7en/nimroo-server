<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Kavenegar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="L5 OpenApi",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="ehs.ghasemi@gmail.com"
     *      ),
     *     @OA\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */


    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'signup', 'sendAcCode',
            'checkActivationCode', 'registerUser']]);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      operationId="LoginUser",
     *      tags={"Auth"},
     *      summary="Login user using username and password",
     *      description="Returns project data",
     *      @OA\Parameter(
     *          name="phone_number",
     *          description="User phone_number",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'phone_number' => 'required|string',
            'password' => 'required'
        ]);
        $credentials = request(['phone_number', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @OA\Get(
     *      path="/api/auth/me",
     *      operationId="Get auth user",
     *      tags={"Auth"},
     *      summary="Get authed user",
     *      description="Returns current authed user",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * @OA\Post(
     *      path="/api/auth/logout",
     *      operationId="LogOut",
     *      tags={"Auth"},
     *      summary="Log the user out (Invalidate the token).",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/refresh",
     *      operationId="RefreshToken",
     *      tags={"Auth"},
     *      summary="Refresh a token.",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => 'bearer ' . $token,
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/signup",
     *      operationId="SignUpUser",
     *      tags={"Auth"},
     *      summary="User sign up",
     *     @OA\Parameter(
     *          name="phone_number",
     *          description="User phone_number",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password_confirmation",
     *          description="User password confirmation",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {
        $request->validate([
            'phone_number' => 'required|unique:users,phone_number',
            'password' => 'bail|required|confirmed',
            'password_confirmation' => 'required'
        ]);
        $user = new User($request->all());
        $user->role_id = Role::where('slug', 'user')->first()->id;
        $user->save();
        $user->token = encrypt($user->id);
        $user->save();
        $credentials = request(['phone_number', 'password']);
        $token = auth()->attempt($credentials);
        return $this->respondWithToken($token);
    }

    /**
     * @OA\Post(
     *      path="/api/auth/activation/check",
     *      operationId="CheckCode",
     *      tags={"Auth"},
     *      summary="Check activation code",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                @OA\Property(property="number",type="integer",),
     *                @OA\Property(property="ac_code",type="integer",),
     *          )
     *         ),
     *     ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkActivationCode(Request $request)
    {
        $request->validate([
            'number' => 'required|unique:users,mobile',
        ]);
        $current_time = strtotime(date("Y-m-d h:i:s"));
        $ac_code = ActivationCode::where([
            'phone_number' => $request->number,
            'activation_code' => $request->ac_code,
            'is_active' => 1
        ])->orderBy('id', 'DESC')->first();
        if ( $ac_code ) {
            if( $ac_code->expiration_time > $current_time ) {
                $password = rand(5000, 6000);
                $role_id = Role::where('slug','user')->first()->id;
                $user = User::create([
                    'mobile' => $request->number,
                    'password' => $password,
                    'role_id' => $role_id
                ]);
                if ( $user ) {
                    return response()->json(['message' => 'verified'], 200);
                }
            }
            else {
                $ac_code->is_active = 0;
                $ac_code->save();
                return response()->json(['message' => 'کد فعال سازی منقضی شده است.'], 403);
            }
        }
        else {
            return response()->json(['message' => 'کد فعال سازی نا معتبر می باشد.'], 403);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/auth/user/profile/update",
     *      operationId="RegisterUser",
     *      tags={"Auth"},
     *      summary="Register new user",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                @OA\Property(property="name",type="string",),
     *                @OA\Property(property="family",type="string",),
     *                @OA\Property(property="email",type="string",),
     *                @OA\Property(property="postal_code",type="string",),
     *                @OA\Property(property="national_code",type="string",),
     *                @OA\Property(property="address",type="string",),
     *          )
     *         ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        $user->update( $request->all() );
        $response = [
            'user' => $user,
            'message' => 'ok',
        ];
        return response()->json($response, 200);
    }
    /**
     * @OA\Post(
     *      path="/api/auth/user/profile/changepass",
     *      operationId="ChangeUserPass",
     *      tags={"Auth"},
     *      summary="Change User Password",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                @OA\Property(property="password",type="string",),
     *                @OA\Property(property="password_confirmation",type="string",),
     *          )
     *         ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        $request->validate([
            'password'=> 'required|confirmed'
        ]);
        $user = auth()->user();
        $user->update( $request->all() );
        return response()->json(['message' => 'Successfully registered']);
    }
}
