<?php

namespace App\Http\Controllers;

use App\Product;
use App\Productcat;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getProductByCat', 'discountedProduct', 'show']]);
    }

    /**
     * @OA\Get(
     *      path="/api/productbycat",
     *      operationId="getProductByCat",
     *      tags={"Product"},
     *      summary="Get Product By Category",
     *      description="Get product form category",
     *     @OA\Parameter(
     *          name="cat_id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductByCat(Request $request)
    {
        $products = Product::where('productcat_id', $request->cat_id)->orderBy('id', 'DESC')->get();
        $response = [
            'products' => $products,
            'message' => 'ok'
        ];
        return response()->json($response, 200);
    }

    /**
     * @OA\Get(
     *      path="/api/product",
     *      operationId="getAllProduct",
     *      tags={"Product"},
     *      summary="Get All Products",
     *      description="Get all product",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $product = Product::orderBy('id', 'DESC')->get();
        $response = [
            'products' => $product,
            'message' => 'ok'
        ];
        return response()->json($response, 200);
    }

    /**
     * @OA\Post(
     *      path="/api/product",
     *      operationId="StoreProduct",
     *      tags={"Product"},
     *      summary="Store product",
     *     @OA\RequestBody(
     *         description="Product object that needs to be added to the store",
     *         required=true,
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                @OA\Property(property="title",type="string",),
     *                 @OA\Property(property="summary",type="string",),
     *                 @OA\Property(property="description",type="string",),
     *                 @OA\Property(property="media_id",type="integer",),
     *                 @OA\Property(property="code",type="string",),
     *                 @OA\Property(property="price",type="integer",),
     *                 @OA\Property(property="inventory",type="integer",),
     *                 @OA\Property(property="productcat_id",type="integer",),
     *                  @OA\Property(
     *                      property="images",
     *                      type="array",
     *                      @OA\Items(type="string")
     *                  )
     *          )
     *         ),
     *     ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $product = Product::create($request->all());
        $userImages = $request->images;
        $product->category;
        $product->cover;
        $product->gallery()->attach($userImages);
        $product->gallery;
        $response = [
            'products' => $product,
            'message' => 'ok',
        ];
        return response()->json($response, 200);
    }

    /**
     * @OA\Get(
     *      path="/api/product/{productId}",
     *      operationId="getProductByID",
     *      tags={"Product"},
     *     @OA\Parameter(
     *          name="productId",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        $response = [
            'product' => $product,
            'message' => 'ok'
        ];
        return response()->json($response, 200);
    }


    /**
     * @OA\Put(
     *      path="/api/product/{product}",
     *      operationId="updateProduct",
     *      tags={"Product"},
     *     @OA\Parameter(
     *          name="product",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *             mediaType="raw",
     *             @OA\Schema(
     *                @OA\Property(property="title",type="string",),
     *                 @OA\Property(property="summary",type="string",),
     *                 @OA\Property(property="description",type="string",),
     *                 @OA\Property(property="media_id",type="integer",),
     *                 @OA\Property(property="code",type="string",),
     *                 @OA\Property(property="price",type="integer",),
     *                 @OA\Property(property="inventory",type="integer",),
     *                 @OA\Property(property="productcat_id",type="integer",),
     *                 @OA\Property(
     *                      property="images",
     *                      type="array",
     *                      @OA\Items(type="string")
     *                  )
     *          )
     *         ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Product $product)
    {
        $product->update( $request->all() );
        $product->gallery()->sync($request->images);
        $response = [
            'product' => $product,
            'message' => 'ok'
        ];
        return response()->json($request->all(), 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/product/{product}",
     *      operationId="deleteProduct",
     *      tags={"Product"},
     *      summary="Delete product",
     *      description="Delete product",
     *     @OA\Parameter(
     *          name="product",
     *          description="Product id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     @OA\Response(response=201, description="Successful created", @OA\JsonContent()),
     *      security={ {"bearer": {}} },
     * )
     * @param Request $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        $data = Product::find($product->id)->delete();
        $response = [
            'data' => $data,
            'message' => 'ok'
        ];
        return response()->json($response, 200);
    }
    public function ranking(User $user)
    {
        $parentStack = [];
        $nodeThreshold = 4;
        $lvl1Point = 100;
        $lvl2point = 75;
        $lvl3point = 50;
        $lvl4point = 25;
        $i = 1;
        if ($user->invite_code) {
            while ($i <= $nodeThreshold) {
                $parentId = decrypt($user->invite_code);
                $parent = User::where('id', $parentId)->first();
                $childs = User::where('invite_code', $user->invite_code)->get()->all();
                if (count($childs) >= 4) {
//                    array_push($parentStack, $parent);
                    $parentStack[$i] = [
                        'node' => $parent,
                        'complete' => true
                    ];
                } else {
                    $parentStack[$i] = [
                        'node' => $parent,
                        'complete' => false
                    ];
                }
                if ($parent->id != decrypt($parent->invite_code)) {
                    break;
                }
                $nextParentId = decrypt($parent->invite_code);
                $user = User::where('id', $nextParentId)->first();
                $i++;
            }
        }
        return response()->json($parentStack, 200);

    }
}
